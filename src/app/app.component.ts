import { MessagingService } from './messaging.service';
import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rnd';
  message;

  myWorkRoutes: ROUTE[] = [
    {
      icon: 'dashboard',
      route: '/academic-wizard',
      title: 'Dashboard',
    }, {
      icon: 'security',
      route: '/assign-module',
      title: 'Modules',
    }
  ];

  constructor(private messagingService: MessagingService, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
  
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;


  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  sendPushNotification(){
    const userId = '2222';
    this.messagingService.requestPermission(userId);
    this.messagingService.receiveMessage();
    this.message = this.messagingService.currentMessage;

    this.messagingService.sendPushMessage("Web push notification", "HI, Firebase test messsage");
  }

  ngOnInit() {}
}

interface ROUTE {
  icon?: string;
  route?: string;
  title?: string;
}